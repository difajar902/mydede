import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Splash from '../pages/splash/Splash';
import Home from '../pages/home/Home';
import Warna from '../pages/warna/Warna';
import Bentuk from '../pages/bentuk/Bentuk';
import Buah from '../pages/buah/Buah';
import Sayur from '../pages/sayur/Sayur';
import Hewan from '../pages/hewan/Hewan';
import Hitam from '../components/pilihwarna/Hitam';
import Putih from '../components/pilihwarna/Putih';
import Biru from '../components/pilihwarna/Biru';
import Hijau from '../components/pilihwarna/Hijau';
import Kuning from '../components/pilihwarna/Kuning';
import Orange from '../components/pilihwarna/Orange';
import Coklat from '../components/pilihwarna/Coklat';
import Pink from '../components/pilihwarna/Pink';
import Ungu from '../components/pilihwarna/Ungu';
import Merah from '../components/pilihwarna/Merah';
import Lingkaran from '../components/pilihbentuk/Lingkaran';
import Pentagram from '../components/pilihbentuk/Pentagram';
import Persegi from '../components/pilihbentuk/Persegi';
import Segienam from '../components/pilihbentuk/Segienam';
import Segilima from '../components/pilihbentuk/Segilima';
import Segitiga from '../components/pilihbentuk/Segitiga';
import Alpukat from '../components/pilihbuah/Alpukat';
import Anggur from '../components/pilihbuah/Anggur';
import Apel from '../components/pilihbuah/Apel'
import Mangga from '../components/pilihbuah/Mangga'
import Pir from '../components/pilihbuah/Pir'
import Jeruk from '../components/pilihbuah/Jeruk'
import Kelapa from '../components/pilihbuah/Kelapa'
import Ceri from '../components/pilihbuah/Ceri'
import Durian from '../components/pilihbuah/Durian'
import Kiwi from '../components/pilihbuah/Kiwi'
import Melon from '../components/pilihbuah/Melon'
import Pepaya from '../components/pilihbuah/Pepaya'
import Pisang from '../components/pilihbuah/Pisang'
import Semangka from '../components/pilihbuah/Semangka'
import Stroberi from '../components/pilihbuah/Stroberi'
import Tomat from '../components/pilihsayur/Tomat'
import Kentang from '../components/pilihsayur/Kentang'
import Wortel from '../components/pilihsayur/Wortel'
import Labu from '../components/pilihsayur/Labu'
import Cabai from '../components/pilihsayur/Cabai'
import Bawangmerah from '../components/pilihsayur/Bawangmerah'
import Bawangputih from '../components/pilihsayur/Bawangputih'
import Bombay from '../components/pilihsayur/Bombay'
import Brokoli from '../components/pilihsayur/Brokoli'
import Buncis from '../components/pilihsayur/Buncis'
import Jagung from '../components/pilihsayur/Jagung'
import Kacangpanjang from '../components/pilihsayur/Kacangpanjang'
import Kacangtanah from '../components/pilihsayur/Kacangtanah'
import Bungakol from '../components/pilihsayur/Bungakol'
import Paprika from '../components/pilihsayur/Paprika'
import Sawi from '../components/pilihsayur/Sawi'
import Terong from '../components/pilihsayur/Terong'
import Timun from '../components/pilihsayur/Timun'
import Ikan from '../components/pilihhewan/Ikan'
import Ayam from '../components/pilihhewan/Ayam'
import Bebek from '../components/pilihhewan/Bebek';
import Anjing from '../components/pilihhewan/Anjing';
import Badak from '../components/pilihhewan/Badak';
import Capung from '../components/pilihhewan/Capung';
import Dinosaurus from '../components/pilihhewan/Dinosaurus';
import Gajah from '../components/pilihhewan/Gajah';
import Harimau from '../components/pilihhewan/Harimau';
import Kelinci from '../components/pilihhewan/Kelinci';
import Kucing from '../components/pilihhewan/Kucing';
import Kupu from '../components/pilihhewan/Kupu';
import Lebah from '../components/pilihhewan/Lebah';
import Panda from '../components/pilihhewan/Panda';
import Pinguin from '../components/pilihhewan/Pinguin';
import Sapi from '../components/pilihhewan/Sapi';
import Semut from '../components/pilihhewan/Semut';
import Singa from '../components/pilihhewan/Singa';
import Ular from '../components/pilihhewan/Ular';
import Zebra from '../components/pilihhewan/Zebra';

const Stack = createStackNavigator();

const Route = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}} />
                <Stack.Screen name="Home" component={Home} options={{headerShown: false}}/>
                <Stack.Screen name="Warna" component={Warna} options={{headerShown: false}}/>
                <Stack.Screen name="Hitam" component={Hitam} options={{headerShown: false}}/>
                <Stack.Screen name="Putih" component={Putih} options={{headerShown: false}}/>
                <Stack.Screen name="Biru" component={Biru} options={{headerShown: false}}/>
                <Stack.Screen name="Kuning" component={Kuning} options={{headerShown: false}}/>
                <Stack.Screen name="Hijau" component={Hijau} options={{headerShown: false}}/>
                <Stack.Screen name="Coklat" component={Coklat} options={{headerShown: false}}/>
                <Stack.Screen name="Merah" component={Merah} options={{headerShown: false}}/>
                <Stack.Screen name="Orange" component={Orange} options={{headerShown: false}}/>
                <Stack.Screen name="Pink" component={Pink} options={{headerShown: false}}/>
                <Stack.Screen name="Ungu" component={Ungu} options={{headerShown: false}}/>
                <Stack.Screen name="Bentuk" component={Bentuk} options={{headerShown: false}}/>
                <Stack.Screen name="Lingkaran" component={Lingkaran} options={{headerShown: false}}/>
                <Stack.Screen name="Pentagram" component={Pentagram} options={{headerShown: false}}/>
                <Stack.Screen name="Persegi" component={Persegi} options={{headerShown: false}}/>
                <Stack.Screen name="Segienam" component={Segienam} options={{headerShown: false}}/>
                <Stack.Screen name="Segilima" component={Segilima} options={{headerShown: false}}/>
                <Stack.Screen name="Segitiga" component={Segitiga} options={{headerShown: false}}/>
                <Stack.Screen name="Buah" component={Buah} options={{headerShown: false}}/>
                <Stack.Screen name="Alpukat" component={Alpukat} options={{headerShown: false}}/>
                <Stack.Screen name="Anggur" component={Anggur} options={{headerShown: false}}/>
                <Stack.Screen name="Apel" component={Apel} options={{headerShown: false}}/>
                <Stack.Screen name="Mangga" component={Mangga} options={{headerShown: false}}/>
                <Stack.Screen name="Pir" component={Pir} options={{headerShown: false}}/>
                <Stack.Screen name="Jeruk" component={Jeruk} options={{headerShown: false}}/>
                <Stack.Screen name="Kelapa" component={Kelapa} options={{headerShown: false}}/>
                <Stack.Screen name="Ceri" component={Ceri} options={{headerShown: false}}/>
                <Stack.Screen name="Durian" component={Durian} options={{headerShown: false}}/>
                <Stack.Screen name="Kiwi" component={Kiwi} options={{headerShown: false}}/>
                <Stack.Screen name="Melon" component={Melon} options={{headerShown: false}}/>
                <Stack.Screen name="Pepaya" component={Pepaya} options={{headerShown: false}}/>
                <Stack.Screen name="Pisang" component={Pisang} options={{headerShown: false}}/>
                <Stack.Screen name="Semangka" component={Semangka} options={{headerShown: false}}/>
                <Stack.Screen name="Stroberi" component={Stroberi} options={{headerShown: false}}/>
                <Stack.Screen name="Sayur" component={Sayur} options={{headerShown: false}}/>
                <Stack.Screen name="Tomat" component={Tomat} options={{headerShown: false}}/>
                <Stack.Screen name="Kentang" component={Kentang} options={{headerShown: false}}/>
                <Stack.Screen name="Wortel" component={Wortel} options={{headerShown: false}}/>
                <Stack.Screen name="Labu" component={Labu} options={{headerShown: false}}/>
                <Stack.Screen name="Cabai" component={Cabai} options={{headerShown: false}}/>
                <Stack.Screen name="Bawangmerah" component={Bawangmerah} options={{headerShown: false}}/>
                <Stack.Screen name="Bawangputih" component={Bawangputih} options={{headerShown: false}}/>
                <Stack.Screen name="Bombay" component={Bombay} options={{headerShown: false}}/>
                <Stack.Screen name="Brokoli" component={Brokoli} options={{headerShown: false}}/>
                <Stack.Screen name="Buncis" component={Buncis} options={{headerShown: false}}/>
                <Stack.Screen name="Jagung" component={Jagung} options={{headerShown: false}}/>
                <Stack.Screen name="Kacangpanjang" component={Kacangpanjang} options={{headerShown: false}}/>
                <Stack.Screen name="Kacangtanah" component={Kacangtanah} options={{headerShown: false}}/>
                <Stack.Screen name="Bungakol" component={Bungakol} options={{headerShown: false}}/>
                <Stack.Screen name="Paprika" component={Paprika} options={{headerShown: false}}/>
                <Stack.Screen name="Sawi" component={Sawi} options={{headerShown: false}}/>
                <Stack.Screen name="Terong" component={Terong} options={{headerShown: false}}/>
                <Stack.Screen name="Timun" component={Timun} options={{headerShown: false}}/>
                <Stack.Screen name="Hewan" component={Hewan} options={{headerShown: false}}/>
                <Stack.Screen name="Ikan" component={Ikan} options={{headerShown: false}}/>
                <Stack.Screen name="Ayam" component={Ayam} options={{headerShown: false}}/>
                <Stack.Screen name="Bebek" component={Bebek} options={{headerShown: false}}/>
                <Stack.Screen name="Anjing" component={Anjing} options={{headerShown: false}}/>
                <Stack.Screen name="Badak" component={Badak} options={{headerShown: false}}/>
                <Stack.Screen name="Capung" component={Capung} options={{headerShown: false}}/>
                <Stack.Screen name="Dinosaurus" component={Dinosaurus} options={{headerShown: false}}/>
                <Stack.Screen name="Gajah" component={Gajah} options={{headerShown: false}}/>
                <Stack.Screen name="Harimau" component={Harimau} options={{headerShown: false}}/>
                <Stack.Screen name="Kelinci" component={Kelinci} options={{headerShown: false}}/>
                <Stack.Screen name="Kucing" component={Kucing} options={{headerShown: false}}/>
                <Stack.Screen name="Kupu" component={Kupu} options={{headerShown: false}}/>
                <Stack.Screen name="Lebah" component={Lebah} options={{headerShown: false}}/>
                <Stack.Screen name="Panda" component={Panda} options={{headerShown: false}}/>
                <Stack.Screen name="Pinguin" component={Pinguin} options={{headerShown: false}}/>
                <Stack.Screen name="Sapi" component={Sapi} options={{headerShown: false}}/>
                <Stack.Screen name="Semut" component={Semut} options={{headerShown: false}}/>
                <Stack.Screen name="Singa" component={Singa} options={{headerShown: false}}/>
                <Stack.Screen name="Ular" component={Ular} options={{headerShown: false}}/>
                <Stack.Screen name="Zebra" component={Zebra} options={{headerShown: false}}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default Route;