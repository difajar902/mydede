import React from 'react'
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity} from 'react-native'
import Ikan from '../../assets/hewan/ikan.png'
import Ayam from '../../assets/hewan/Ayam.png'
import Bebek from '../../assets/hewan/bebek.jpeg'
import Anjing from '../../assets/hewan/anjing.jpg'
import Badak from '../../assets/hewan/badak.jpg'
import Capung from '../../assets/hewan/capung.jpg'
import Dinosaurus from '../../assets/hewan/dinosaurus.png'
import Gajah from '../../assets/hewan/gajah.jpg'
import Harimau from '../../assets/hewan/harimau.jpg'
import Kelinci from '../../assets/hewan/kelinci.jpg'
import Kucing from '../../assets/hewan/kucing.jpg'
import Kupu from '../../assets/hewan/kupu.jpg'
import Lebah from '../../assets/hewan/lebah.jpg'
import Panda from '../../assets/hewan/panda.jpg'
import Pinguin from '../../assets/hewan/pinguin.jpg'
import Sapi from '../../assets/hewan/sapi.jpg'
import Semut from '../../assets/hewan/semut.webp'
import Singa from '../../assets/hewan/singa.jpg'
import Ular from '../../assets/hewan/ular.jpg'
import Zebra from '../../assets/hewan/zebra.png'

const Hewan = ({navigation}) => {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Ikan')}>
                        <Image source={Ikan} style={styles.ikan}/>
                        <Text style={styles.text}>Ikan</Text>
                        <Text style={styles.text2}>(Fish)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Ayam')}>
                        <Image source={Ayam} style={styles.ayam}/>
                        <Text style={styles.text3}>Ayam</Text>
                        <Text style={styles.text4}>(Chicken)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Bebek')}>
                        <Image source={Bebek} style={styles.bebek}/>
                        <Text style={styles.text5}>Bebek</Text>
                        <Text style={styles.text6}>(Duck)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Anjing')}>
                        <Image source={Anjing} style={styles.anjing}/>
                        <Text style={styles.text7}>Anjing</Text>
                        <Text style={styles.text8}>(Dog)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Badak')}>
                        <Image source={Badak} style={styles.badak}/>
                        <Text style={styles.text9}>Badak</Text>
                        <Text style={styles.text10}>(Rhino)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Capung')}>
                        <Image source={Capung} style={styles.capung}/>
                        <Text style={styles.text11}>Capung</Text>
                        <Text style={styles.text12}>(Dragonfly)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Dinosaurus')}>
                        <Image source={Dinosaurus} style={styles.dinosaurus}/>
                        <Text style={styles.text13}>Dinosaurus</Text>
                        <Text style={styles.text14}>(Dinosaur)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Gajah')}>
                        <Image source={Gajah} style={styles.gajah}/>
                        <Text style={styles.text15}>Gajah</Text>
                        <Text style={styles.text16}>(Elephant)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Harimau')}>
                        <Image source={Harimau} style={styles.harimau}/>
                        <Text style={styles.text17}>Harimau</Text>
                        <Text style={styles.text18}>(Tiger)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Kelinci')}>
                        <Image source={Kelinci} style={styles.kelinci}/>
                        <Text style={styles.text19}>Kelinci</Text>
                        <Text style={styles.text20}>(Rabbit)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Kucing')}>
                        <Image source={Kucing} style={styles.kucing}/>
                        <Text style={styles.text21}>Kucing</Text>
                        <Text style={styles.text22}>(Cat)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Kupu')}>
                        <Image source={Kupu} style={styles.kupu}/>
                        <Text style={styles.text23}>Kupu-kupu</Text>
                        <Text style={styles.text24}>(Butterfly)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Lebah')}>
                        <Image source={Lebah} style={styles.lebah}/>
                        <Text style={styles.text25}>Lebah</Text>
                        <Text style={styles.text26}>(Bee)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Panda')}>
                        <Image source={Panda} style={styles.panda}/>
                        <Text style={styles.text27}>Panda</Text>
                        <Text style={styles.text28}>(Panda)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Pinguin')}>
                        <Image source={Pinguin} style={styles.pinguin}/>
                        <Text style={styles.text29}>Pinguin</Text>
                        <Text style={styles.text30}>(Penguin)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Sapi')}>
                        <Image source={Sapi} style={styles.sapi}/>
                        <Text style={styles.text31}>Sapi</Text>
                        <Text style={styles.text32}>(Cow)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Semut')}>
                        <Image source={Semut} style={styles.semut}/>
                        <Text style={styles.text33}>Semut</Text>
                        <Text style={styles.text34}>(Ant)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Singa')}>
                        <Image source={Singa} style={styles.singa}/>
                        <Text style={styles.text35}>Singa</Text>
                        <Text style={styles.text36}>(Lion)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Ular')}>
                        <Image source={Ular} style={styles.ular}/>
                        <Text style={styles.text37}>Ular</Text>
                        <Text style={styles.text38}>(Snake)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Zebra')}>
                        <Image source={Zebra} style={styles.zebra}/>
                        <Text style={styles.text39}>Zebra</Text>
                        <Text style={styles.text40}>(Zebra)</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 50,
        backgroundColor: 'white'
    },
    ikan: {
        width: 200,
        height: 140,
        margin: 25,
        marginBottom: 1,
    },
    ayam: {
        width: 182,
        height: 140,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    bebek: {
        width: 180,
        height: 145,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    anjing: {
        width: 180,
        height: 180,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    badak: {
        width: 190,
        height: 140,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    capung: {
        width: 182,
        height: 140,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    dinosaurus: {
        width: 200,
        height: 130,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    gajah: {
        width: 200,
        height: 140,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    harimau: {
        width: 200,
        height: 175,
        margin: 25,
        marginBottom: 1,
    },
    kelinci: {
        width: 182,
        height: 140,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    kucing: {
        width: 182,
        height: 140,
        margin: 25,
        marginBottom: 1,
    },
    kupu: {
        width: 160,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 75
    },
    lebah: {
        width: 170,
        height: 110,
        margin: 25,
        marginBottom: 1,
        marginLeft: 55
    },
    panda: {
        width: 190,
        height: 145,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    pinguin: {
        width: 190,
        height: 170,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    sapi: {
        width: 200,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 30
    },
    semut: {
        width: 182,
        height: 140,
        margin: 25,
        marginBottom: 1,
        marginLeft: 70
    },
    singa: {
        width: 210,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    ular: { 
        width: 190,
        height: 110,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    zebra: {
        width: 192,
        height: 145,
        margin: 25,
        marginBottom: 1,
    },
    text: {
        textAlign: 'center',
        fontSize: 20,
    },
    text2: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text3: {
        textAlign: 'center',
        fontSize: 20
    },
    text4: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text5: {
        textAlign: 'center',
        fontSize: 20
    },
    text6: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text7: {
        textAlign: 'center',
        fontSize: 20
    },
    text8: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text9: {
        textAlign: 'center',
        fontSize: 20
    },
    text10: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text11: {
        textAlign: 'center',
        fontSize: 20
    },
    text12: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text13: {
        textAlign: 'center',
        fontSize: 20
    },
    text14: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text15: {
        textAlign: 'center',
        fontSize: 20
    },
    text16: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text17: {
        textAlign: 'center',
        fontSize: 20,
    },
    text18: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text19: {
        textAlign: 'center',
        fontSize: 20
    },
    text20: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text21: {
        textAlign: 'center',
        fontSize: 20
    },
    text22: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text23: {
        textAlign: 'center',
        fontSize: 20
    },
    text24: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text25: {
        textAlign: 'center',
        fontSize: 20
    },
    text26: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text27: {
        textAlign: 'center',
        fontSize: 20,
    },
    text28: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text29: {
        textAlign: 'center',
        fontSize: 20
    },
    text30: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text31: {
        textAlign: 'center',
        fontSize: 20
    },
    text32: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text33: {
        textAlign: 'center',
        fontSize: 20
    },
    text34: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text35: {
        textAlign: 'center',
        fontSize: 20
    },
    text36: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text37: {
        textAlign: 'center',
        fontSize: 20
    },
    text38: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text39: {
        textAlign: 'center',
        fontSize: 20
    },
    text40: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
})

export default Hewan
