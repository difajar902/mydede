import React from 'react'
import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity} from 'react-native'
import Lingkaran from '../../assets/bentuk/Lingkaran.jpg'
import Pentagram from '../../assets/bentuk/Pentagram.jpg'
import Persegi from '../../assets/bentuk/Persegi.jpg'
import Segienam from '../../assets/bentuk/Segienam.jpg'
import Segilima from '../../assets/bentuk/Segilima.jpg'
import Segitiga from '../../assets/bentuk/Segitiga.jpg'

const Bentuk = ({navigation}) => {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Lingkaran')}>
                        <Image source={Lingkaran} style={styles.lingkaran}/>
                        <Text style={styles.text}>Lingkaran</Text>
                        <Text style={styles.text2}>(Circle)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Pentagram')}>
                        <Image source={Pentagram} style={styles.pentagram} />
                        <Text style={styles.text3}>Pentagram</Text>
                        <Text style={styles.text4}>(Pentagram)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Persegi')}>
                        <Image source={Persegi} style={styles.persegi}/>
                        <Text style={styles.text5}>Persegi</Text>
                        <Text style={styles.text6}>(Square)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Segienam')}>
                        <Image source={Segienam} style={styles.segienam}/>
                        <Text style={styles.text7}>Segienam</Text>
                        <Text style={styles.text8}>(Hexagon)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Segilima')}>
                        <Image source={Segilima} style={styles.segilima}/>
                        <Text style={styles.text9}>Segilima</Text>
                        <Text style={styles.text10}>(Pentagon)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Segitiga')}>
                        <Image source={Segitiga} style={styles.segitiga}/>
                        <Text style={styles.text11}>Segitiga</Text>
                        <Text style={styles.text12}>(Triangle)</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 50,
        backgroundColor: 'white'
    },
    lingkaran: {
        width: 150,
        height: 150,
        margin: 25,
        marginBottom: 1
    },
    pentagram: {
        width: 150,
        height: 150,
        margin: 25,
        marginBottom: 1
    },
    persegi: {
        width: 150,
        height: 150,
        margin: 25,
        marginBottom: 1
    },
    segienam: {
        width: 150,
        height: 150,
        margin: 25,
        marginBottom: 1
    },
    segilima: {
        width: 150,
        height: 150,
        margin: 25,
        marginBottom: 1
    },
    segitiga: {
        width: 150,
        height: 140,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
    },
    text2: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text3: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text4: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text5: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text6: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text7: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text8: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text9: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text10: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text11: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text12: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    }
})

export default Bentuk
