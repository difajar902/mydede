import React from 'react'
import { View, Text, ScrollView, Image, StyleSheet, TouchableOpacity } from 'react-native'
import Tomat from '../../assets/sayur/tomat.png'
import Cabai from '../../assets/sayur/cabai.png'
import Kentang from '../../assets/sayur/kentang.png'
import Wortel from '../../assets/sayur/wortel.png'
import Labu from '../../assets/sayur/labu.png'
import Bawangmerah from '../../assets/sayur/bawangmerah.jpg'
import Bawangputih from '../../assets/sayur/bawangputih.png'
import Bombay from '../../assets/sayur/bombay.jpg'
import Brokoli from '../../assets/sayur/brokoli.jpg'
import Buncis from '../../assets/sayur/buncis.jpg'
import Jagung from '../../assets/sayur/jagung.png'
import Kacangpanjang from '../../assets/sayur/kacangpanjang.jpg'
import Kacangtanah from '../../assets/sayur/kacangtanah.jpg'
import Kembangkol from '../../assets/sayur/kembangkol.jpg'
import Paprika from '../../assets/sayur/paprika.jpg'
import Sawi from '../../assets/sayur/sawi.jpg'
import Terong from '../../assets/sayur/terong.jpg'
import Timun from '../../assets/sayur/timun.jpg'

const Sayur = ({navigation}) => {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Tomat')}>
                        <Image source={Tomat} style={styles.tomat} />
                        <Text style={styles.text}>Tomat</Text>
                        <Text style={styles.text2}>(Tomato)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Kentang')}>
                        <Image source={Kentang} style={styles.kentang} />
                        <Text style={styles.text3}>Kentang</Text>
                        <Text style={styles.text4}>(Potato)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                   <TouchableOpacity onPress={() => navigation.navigate('Wortel')}>
                        <Image source={Wortel} style={styles.wortel} />
                        <Text style={styles.text5}>Wortel</Text>
                        <Text style={styles.text6}>(Carrot)</Text>
                   </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Labu')}>
                        <Image source={Labu} style={styles.labu} />
                        <Text style={styles.text7}>Labu</Text>
                        <Text style={styles.text8}>(Pumpkin)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Cabai')}>
                        <Image source={Cabai} style={styles.cabai} />
                        <Text style={styles.text9}>Cabai</Text>
                        <Text style={styles.text10}>(Chili)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Bawangmerah')}>
                        <Image source={Bawangmerah} style={styles.bawangmerah} />
                        <Text style={styles.text11}>Bawang Merah</Text>
                        <Text style={styles.text12}>(Shallot)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Bawangputih')}>
                        <Image source={Bawangputih} style={styles.bawangputih} />
                        <Text style={styles.text13}>Bawang Putih</Text>
                        <Text style={styles.text14}>(Garlic)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Bombay')}>
                        <Image source={Bombay} style={styles.bombay} />
                        <Text style={styles.text15}>Bawang Bombay</Text>
                        <Text style={styles.text16}>(Onion)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Brokoli')}>
                        <Image source={Brokoli} style={styles.brokoli} />
                        <Text style={styles.text17}>Brokoli</Text>
                        <Text style={styles.text18}>(Broccoli)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Buncis')}>
                        <Image source={Buncis} style={styles.buncis} />
                        <Text style={styles.text19}>Buncis</Text>
                        <Text style={styles.text20}>(Snaps)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Jagung')}>
                        <Image source={Jagung} style={styles.jagung} />
                        <Text style={styles.text21}>jagung</Text>
                        <Text style={styles.text22}>(Corn)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Kacangpanjang')}>
                        <Image source={Kacangpanjang} style={styles.kacangpanjang} />
                        <Text style={styles.text23}>Kacang Panjang</Text>
                        <Text style={styles.text24}>(Long Bean)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Kacangtanah')}>
                        <Image source={Kacangtanah} style={styles.kacangtanah} />
                        <Text style={styles.text25}>Kacang Tanah</Text>
                        <Text style={styles.text26}>(Peanut)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Bungakol')}>
                        <Image source={Kembangkol} style={styles.kembangkol} />
                        <Text style={styles.text27}>Bunga Kol</Text>
                        <Text style={styles.text28}>(Cauliflower)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Paprika')}>
                        <Image source={Paprika} style={styles.paprika} />
                        <Text style={styles.text29}>Paprika</Text>
                        <Text style={styles.text30}>(Paprika)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Sawi')}>
                        <Image source={Sawi} style={styles.sawi} />
                        <Text style={styles.text31}>Sawi Hijau</Text>
                        <Text style={styles.text32}>(Mustard Green)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Terong')}>
                        <Image source={Terong} style={styles.terong} />
                        <Text style={styles.text33}>Terong</Text>
                        <Text style={styles.text34}>(Eggplant)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={() => navigation.navigate('Timun')}>
                        <Image source={Timun} style={styles.timun} />
                        <Text style={styles.text35}>Timun</Text>
                        <Text style={styles.text36}>(Cucumber)</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 50,
        backgroundColor: 'white'
        
    },
    bawangmerah: {
        width: 182,
        height: 140,
        margin: 25,
        marginBottom: 1,
    },
    bawangputih: {
        width: 182,
        height: 120,
        margin: 25,
        marginBottom: 1,
    },
    bombay: {
        width: 185,
        height: 130,
        margin: 25,
        marginBottom: 1,
    },
    brokoli: {
        width: 160,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    buncis: {
        width: 160,
        height: 120,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    cabai: {
        width: 180,
        height: 140,
        margin: 25,
        marginBottom: 1,
    },
    jagung: {
        width: 170,
        height: 170,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    kacangpanjang: {
        width: 185,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    kacangtanah: {
        width: 160,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    kembangkol: {
        width: 180,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    kentang: {
        width: 160,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    labu: {
        width: 200,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 30
    },
    paprika: {
        width: 200,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 30
    },
    sawi: {
        width: 190,
        height: 150,
        margin: 25,
        marginBottom: 1,
    },
    terong: {
        width: 170,
        height: 170,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    timun: {
        width: 210,
        height: 121,
        margin: 25,
        marginBottom: 1,
    },
    tomat: {
        width: 160,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    wortel: {
        width: 180,
        height: 110,
        margin: 25,
        marginBottom: 1,
    },
    text: {
        textAlign: 'center',
        fontSize: 20,
    },
    text2: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text3: {
        textAlign: 'center',
        fontSize: 20
    },
    text4: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text5: {
        textAlign: 'center',
        fontSize: 20
    },
    text6: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text7: {
        textAlign: 'center',
        fontSize: 20
    },
    text8: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text9: {
        textAlign: 'center',
        fontSize: 20
    },
    text10: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text11: {
        textAlign: 'center',
        fontSize: 20
    },
    text12: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text13: {
        textAlign: 'center',
        fontSize: 20
    },
    text14: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text15: {
        textAlign: 'center',
        fontSize: 20
    },
    text16: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text17: {
        textAlign: 'center',
        fontSize: 20,
    },
    text18: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text19: {
        textAlign: 'center',
        fontSize: 20
    },
    text20: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text21: {
        textAlign: 'center',
        fontSize: 20
    },
    text22: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text23: {
        textAlign: 'center',
        fontSize: 20
    },
    text24: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text25: {
        textAlign: 'center',
        fontSize: 20
    },
    text26: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text27: {
        textAlign: 'center',
        fontSize: 20,
    },
    text28: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text29: {
        textAlign: 'center',
        fontSize: 20
    },
    text30: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text31: {
        textAlign: 'center',
        fontSize: 20
    },
    text32: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text33: {
        textAlign: 'center',
        fontSize: 20
    },
    text34: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text35: {
        textAlign: 'center',
        fontSize: 20
    },
    text36: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
})

export default Sayur
