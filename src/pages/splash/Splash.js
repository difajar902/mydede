import React,{useEffect} from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Logo from '../../assets/adat.jpg'

const Splash = ({navigation}) => {
    useEffect (() => {
        setTimeout(() => {
            navigation.replace('Home');
        }, 5000);
    });
    return (
        <View style={styles.container}>
            <Image source={Logo} style={styles.logo}/>
            <Text style={styles.text1}>Dede</Text>
            <Text style={styles.text}>Basic Learning</Text>
            <Text style={styles.text3}>For Children</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 190,
        height: 170,
        marginTop: 60,
    },
    text1: {
        fontSize: 20,
        color: 'gray',
        marginTop: 220
    },
    text: {
        fontSize: 25,
        color: 'green',
    },
    text3: {
        fontSize: 20,
        color: 'green',
    }
});
export default Splash;