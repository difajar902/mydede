import React from 'react';
import { View, Text,TouchableOpacity, ImageBackground } from 'react-native';

const Home = ({navigation}) => {
    return (
        <ImageBackground source={require('../../assets/background1.jpg')} style={{flex:1, flexDirection:'column', backgroundColor: 'transparent'}}>
            <View style={{flex: 1, alignItems: 'center', paddingTop: 100}}>
            <TouchableOpacity onPress={() => navigation.navigate('Warna')}>
                <View style={{width: 300, height: 50, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', margin: 10, marginTop: 40}}>
                    <Text style={{fontSize: 16,}}>
                        W   A   R   N   A
                    </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Bentuk')}>
                <View style={{width: 300, height: 50, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', margin: 10}}>
                    <Text style={{fontSize: 16}}>
                        B   E   N   T   U   K
                    </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Buah')}>
                <View style={{width: 300, height: 50, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', margin: 10}}>
                    <Text style={{fontSize: 16}}>
                        B   U   A   H
                    </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Sayur')}>
                <View style={{width: 300, height: 50, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', margin: 10}}>
                    <Text style={{fontSize: 16}}>
                        S   A   Y   U   R
                    </Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Hewan')}>
                <View style={{width: 300, height: 50, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', margin: 10}}>
                    <Text style={{fontSize: 16}}>
                        H   E   W   A   N
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
        </ImageBackground>
    )
}
export default Home;