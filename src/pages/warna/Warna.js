import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native';
import Hitam from '../../assets/warna/hitam.jpg'
import Putih from '../../assets/warna/putih.jpg'
import Biru from '../../assets/warna/biru.png'
import Coklat from '../../assets/warna/coklat.png'
import Hijau from '../../assets/warna/hijau.jpg'
import Kuning from '../../assets/warna/kuning.jpg'
import Merah from '../../assets/warna/merah.jpg'
import Orange from '../../assets/warna/orange.png'
import Pink from '../../assets/warna/pink.jpeg'
import Ungu from '../../assets/warna/ungu.jpg'

const Warna = ({navigation}) => {
    return (
        <View style={styles.container}>
            <ScrollView>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Hitam')}>
                            <Image source={Hitam} style={styles.hitam}/>
                            <Text style={styles.text}>Hitam</Text>
                            <Text style={styles.text2}>(Black)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Putih')}>
                            <Image source={Putih} style={styles.putih}/>
                            <Text style={styles.text3}>Putih</Text>
                            <Text style={styles.text4}>(White)</Text>   
                        </TouchableOpacity> 
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Biru')}>
                            <Image source={Biru} style={styles.biru}/>
                            <Text style={styles.text5}>Biru</Text>
                            <Text style={styles.text6}>(Blue)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Kuning')}>
                            <Image source={Kuning} style={styles.kuning}/>
                            <Text style={styles.text7}>Kuning</Text>
                            <Text style={styles.text8}>(Yellow)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Hijau')}>
                            <Image source={Hijau} style={styles.hijau}/>
                            <Text style={styles.text9}>Hijau</Text>
                            <Text style={styles.text10}>(Green)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Coklat')}>
                            <Image source={Coklat} style={styles.coklat}/>
                            <Text style={styles.text11}>Coklat</Text>
                            <Text style={styles.text12}>(Chocolate)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Merah')}>
                            <Image source={Merah} style={styles.merah}/>
                            <Text style={styles.text13}>Merah</Text>
                            <Text style={styles.text14}>(Red)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Orange')}>
                            <Image source={Orange} style={styles.orange}/>
                            <Text style={styles.text15}>Orange</Text>
                            <Text style={styles.text16}>(Orange)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Pink')}>
                            <Image source={Pink} style={styles.pink}/>
                            <Text style={styles.text17}>Merah Muda</Text>
                            <Text style={styles.text18}>(Pink)</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => navigation.navigate('Ungu')}>
                            <Image source={Ungu} style={styles.ungu}/>
                            <Text style={styles.text19}>Ungu</Text>
                            <Text style={styles.text20}>(Indigo)</Text>
                        </TouchableOpacity>
                    </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 50
    },
    hitam: {
        borderRadius: 50,
        width: 100,
        height: 100,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
    },
    text2: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text3: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text4: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text5: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text6: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text7: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text8: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text9: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text10: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text11: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text12: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text13: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text14: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text15: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text16: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text17: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text18: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    text19: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20
    },
    text20: {
        textAlign: 'center',
        fontFamily: 'bold',
        fontSize: 20,
        marginBottom: 50
    },
    putih: {
        marginTop: 35,
        marginLeft: 16,
        borderRadius: 50,
        width: 100,
        height: 100,

    },
    biru: {
        marginTop: 20,
        marginLeft: 17,
        borderRadius: 50,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    kuning: {
        borderRadius: 50,
        marginLeft: 18,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    hijau: {
        borderRadius: 50,
        marginLeft: 19,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    coklat: {
        marginTop: 20,
        marginLeft: 20,
        borderRadius: 50,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    merah: {
        borderRadius: 50,
        marginLeft: 20,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    orange: {
        borderRadius: 50,
        marginLeft: 20,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    pink : {
        borderRadius: 50,
        marginLeft: 20,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    },
    ungu: {
        borderRadius: 50,
        marginLeft: 20,
        width: 100,
        height: 100,
        margin: 10,
        marginBottom: 1
    }
})
export default Warna;