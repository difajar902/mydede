import React from 'react'
import { View, Text, StyleSheet, Image, ScrollView, TouchableOpacity } from 'react-native'
import Alpukat from '../../assets/buah/alpukat.png'
import Anggur from '../../assets/buah/anggur.png'
import Apel from '../../assets/buah/apel.png'
import Cherry from '../../assets/buah/ceri.png'
import Durian from '../../assets/buah/durian.jpg'
import Jeruk from '../../assets/buah/jeruk.jpg'
import Kelapa from '../../assets/buah/kelapa.png'
import Kiwi from '../../assets/buah/kiwi.png'
import Mangga from '../../assets/buah/mangga.png'
import Melon from '../../assets/buah/melon.jpg'
import Pepaya from '../../assets/buah/pepaya1.jpg'
import Pir from '../../assets/buah/pir.png'
import Pisang from '../../assets/buah/pisang.jpg'
import Semangka from '../../assets/buah/semangka.webp'
import Strawberry from '../../assets/buah/strawberry.jpg'

const Buah = ({navigation}) => {
    return (
        <View style={styles.container}>
            <ScrollView>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Alpukat')}>
                        <Image source={Alpukat} style={styles.alpukat}/>
                        <Text style={styles.text01}>Alpukat</Text>
                        <Text style={styles.text02}>(Avocado)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Anggur')}>
                        <Image source={Anggur} style={styles.anggur}/>
                        <Text style={styles.text03}>Anggur</Text>
                        <Text style={styles.text04}>(Grape)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Apel')}>
                        <Image source={Apel} style={styles.apel}/>
                        <Text style={styles.text05}>Apel</Text>
                        <Text style={styles.text06}>(Apple)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Mangga')}>
                        <Image source={Mangga} style={styles.mangga}/>
                        <Text style={styles.text07}>Mangga</Text>
                        <Text style={styles.text08}>(Mango)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Pir')}>
                        <Image source={Pir} style={styles.pir}/>
                        <Text style={styles.text09}>Pir</Text>
                        <Text style={styles.text00}>(Pear)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Jeruk')}>
                        <Image source={Jeruk} style={styles.jeruk}/>
                        <Text style={styles.text001}>Jeruk</Text>
                        <Text style={styles.text002}>(Orange)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Kelapa')}>
                        <Image source={Kelapa} style={styles.kelapa}/>
                        <Text style={styles.text003}>Kelapa</Text>
                        <Text style={styles.text004}>(Apple)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Ceri')}>
                        <Image source={Cherry} style={styles.cherry}/>
                        <Text style={styles.text}>Ceri</Text>
                        <Text style={styles.text2}>(Cherry)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Durian')}>
                        <Image source={Durian} style={styles.durian}/>
                        <Text style={styles.text3}>Durian</Text>
                        <Text style={styles.text4}>(Durian)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Kiwi')}>
                        <Image source={Kiwi} style={styles.kiwi}/>
                        <Text style={styles.text5}>Kiwi</Text>
                        <Text style={styles.text6}>(Kiwi)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Melon')}>
                        <Image source={Melon} style={styles.melon}/>
                        <Text style={styles.text7}>Melon</Text>
                        <Text style={styles.text8}>(Melon)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Pepaya')}>
                        <Image source={Pepaya} style={styles.pepaya}/>
                        <Text style={styles.text9}>Pepaya</Text>
                        <Text style={styles.text10}>(Papaya)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Pisang')}>
                        <Image source={Pisang} style={styles.pisang}/>
                        <Text style={styles.text11}>Pisang</Text>
                        <Text style={styles.text12}>(Banana)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Semangka')}>
                        <Image source={Semangka} style={styles.semangka}/>
                        <Text style={styles.text13}>Semangka</Text>
                        <Text style={styles.text14}>(Watermelon)</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity onPress={()=> navigation.navigate('Stroberi')}>
                        <Image source={Strawberry} style={styles.strawberry}/>
                        <Text style={styles.text15}>Stroberi</Text>
                        <Text style={styles.text16}>(Strawberry)</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 50,
        backgroundColor: 'white'
    },
    cherry: {
        width: 170,
        height: 120,
        margin: 25,
        marginBottom: 1,
        marginLeft: 60
    },
    durian: {
        width: 200,
        height: 110,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    kiwi: {
        width: 200,
        height: 130,
        margin: 25,
        marginLeft: 35,
        marginBottom: 1
    },
    melon: {
        width: 160,
        height: 160,
        margin: 25,
        marginLeft: 50,
        marginBottom: 1
    },
    pepaya: {
        width: 170,
        height: 150,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    pisang: {
        width: 180,
        height: 120,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    semangka: {
        width: 200,
        height: 120,
        margin: 25,
        marginBottom: 1,
        marginLeft: 38
    },
    strawberry: {
        width: 200,
        height: 110,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    apel: {
        width: 170,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    anggur: {
        width: 170,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    jeruk: {
        width: 180,
        height: 120,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    pir: {
        width: 170,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 35
    },
    kelapa: {
        width: 170,
        height: 130,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    alpukat: {
        width: 170,
        height: 130,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    mangga: {
        width: 170,
        height: 160,
        margin: 25,
        marginBottom: 1,
        marginLeft: 40
    },
    text01: {
        textAlign: 'center',
        fontSize: 20,
    },
    text02: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text03: {
        textAlign: 'center',
        fontSize: 20
    },
    text04: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text05: {
        textAlign: 'center',
        fontSize: 20
    },
    text06: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text07: {
        textAlign: 'center',
        fontSize: 20
    },
    text08: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text09: {
        textAlign: 'center',
        fontSize: 20
    },
    text00: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text001: {
        textAlign: 'center',
        fontSize: 20
    },
    text002: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text003: {
        textAlign: 'center',
        fontSize: 20
    },
    text004: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text: {
        textAlign: 'center',
        fontSize: 20,
    },
    text2: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text3: {
        textAlign: 'center',
        fontSize: 20
    },
    text4: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text5: {
        textAlign: 'center',
        fontSize: 20
    },
    text6: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text7: {
        textAlign: 'center',
        fontSize: 20
    },
    text8: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text9: {
        textAlign: 'center',
        fontSize: 20
    },
    text10: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text11: {
        textAlign: 'center',
        fontSize: 20
    },
    text12: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text13: {
        textAlign: 'center',
        fontSize: 20
    },
    text14: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    },
    text15: {
        textAlign: 'center',
        fontSize: 20
    },
    text16: {
        textAlign: 'center',
        fontSize: 20,
        marginBottom: 50
    }
})

export default Buah
