import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Square from '../../assets/bentuk/Persegi.jpg'

const Persegi = () => {
    return (
        <View style={styles.container}>
            <Image source={Square} style={styles.persegi}/>
            <Text style={styles.text}>Persegi</Text>
            <Text style={styles.text2}>(Square)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    persegi: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})

export default Persegi
