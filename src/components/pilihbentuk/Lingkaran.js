import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Circle from '../../assets/bentuk/Lingkaran.jpg'

const Lingkaran = () => {
    return (
            <View style={styles.container}>
                <Image source={Circle} style={styles.lingkaran}/>
                <Text style={styles.text}>Lingkaran</Text>
                <Text style={styles.text2}>(Circle)</Text>
            </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    lingkaran: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Lingkaran
