import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Hexagon from '../../assets/bentuk/Segienam.jpg'

const Segienam = () => {
    return (
        <View style={styles.container}>
            <Image source={Hexagon} style={styles.segienam}/>
            <Text style={styles.text}>Segienam</Text>
            <Text style={styles.text2}>(Hexagon)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    segienam: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})

export default Segienam
