import React from 'react'
import { View, Text,Image, StyleSheet } from 'react-native'
import Penta from '../../assets/bentuk/Pentagram.jpg'

const Pentagram = () => {
    return (
        <View style={styles.container}>
            <Image source={Penta} style={styles.pentagram}/>
            <Text style={styles.text}>Pentagram</Text>
            <Text style={styles.text2}>(Pentagram)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    pentagram: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Pentagram
