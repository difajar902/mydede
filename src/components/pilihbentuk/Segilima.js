import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Pentagon from '../../assets/bentuk/Segilima.jpg'

const Segilima = () => {
    return (
        <View style={styles.container}>
            <Image source={Pentagon} style={styles.segilima}/>
            <Text style={styles.text}>Segilima</Text>
            <Text style={styles.text2}>(Pentagon)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    segilima: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})

export default Segilima
