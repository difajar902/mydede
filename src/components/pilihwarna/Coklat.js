import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Chocolate from '../../assets/warna/coklat.png'

const Coklat = () => {
    return (
        <View style={styles.container}>
            <Image source={Chocolate} style={styles.coklat}/>
            <Text style={styles.text}>Coklat</Text>
            <Text style={styles.text2}>(Chocolate)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    coklat: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Coklat
