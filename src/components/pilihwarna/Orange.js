import React from 'react'
import { View, Text, Image, StyleSheet} from 'react-native'
import Or from '../../assets/warna/orange.png'

const Orange = () => {
    return (
        <View style={styles.container}>
            <Image source={Or} style={styles.orange}/>
            <Text style={styles.text}>Orange</Text>
            <Text style={styles.text2}>(Orange)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    orange: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Orange
