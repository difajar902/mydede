import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Blue from '../../assets/warna/biru.png'

const Biru = () => {
    return (
        <View style={styles.container}>
            <Image source={Blue} style={styles.biru}/>
            <Text style={styles.text}>Biru</Text>
            <Text style={styles.text2}>(Blue)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    biru: {
        borderRadius: 1100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Biru
