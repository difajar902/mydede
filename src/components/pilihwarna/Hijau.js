import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Green from '../../assets/warna/hijau.jpg'

const Hijau = () => {
    return (
        <View style={styles.container}>
            <Image source={Green} style={styles.hijau}/>
            <Text style={styles.text}>Hijau</Text>
            <Text style={styles.text2}>(Green)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    hijau: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Hijau
