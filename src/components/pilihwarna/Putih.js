import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import White from '../../assets/warna/putih.jpg'

const Putih = () => {
    return (
        <View style={styles.container}>
            <Image source={White} style={styles.white}/>
            <Text style={styles.text}>Putih</Text>
            <Text style={styles.text2}>(White)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    white: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Putih
