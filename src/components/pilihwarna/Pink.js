import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Merahmuda from '../../assets/warna/pink.jpeg'

const Pink = () => {
    return (
        <View style={styles.container}>
            <Image source={Merahmuda} style={styles.merahmuda}/>
            <Text style={styles.text}>Merah Muda</Text>
            <Text style={styles.text2}>(Pink)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    merahmuda: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Pink
