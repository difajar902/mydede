import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Black from '../../assets/warna/hitam.jpg'

const Hitam = () => {
    return (
        <View style={styles.container}>
            <Image source={Black} style={styles.black}/>
            <Text style={styles.text}>Hitam</Text>
            <Text style={styles.text2}>(Black)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    black: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Hitam
