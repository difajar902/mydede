import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Red from '../../assets/warna/merah.jpg'

const Merah = () => {
    return (
        <View style={styles.container}>
            <Image source={Red} style={styles.merah}/>
            <Text style={styles.text}>Merah</Text>
            <Text style={styles.text2}>(Red)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    merah: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})

export default Merah
