import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Indigo from '../../assets/warna/ungu.jpg'

const Ungu = () => {
    return (
        <View style={styles.container}>
            <Image source={Indigo} style={styles.ungu}/>
            <Text style={styles.text}>Ungu</Text>
            <Text style={styles.text2}>(Indigo)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150
    },
    ungu: {
        borderRadius: 100,
        width: 200,
        height: 200,
        margin: 15,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
    }    
})


export default Ungu
