import React from 'react'
import { View, Text,Image, StyleSheet } from 'react-native'
import Dinosaur from '../../assets/hewan/dinosaurus.png'

const Dinosaurus = () => {
    return (
        <View style={styles.container}>
        <Image source={Dinosaur} style={styles.dinosaurus}/>
        <Text style={styles.text}>Dinosaurus</Text>
        <Text style={styles.text2}>(Dinosaur)</Text>
    </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    dinosaurus: {
        width: 335,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Dinosaurus
