import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Dragonfly from '../../assets/hewan/capung.jpg'

const Capung = () => {
    return (
        <View style={styles.container}>
            <Image source={Dragonfly} style={styles.capung}/>
            <Text style={styles.text}>Capung</Text>
            <Text style={styles.text2}>(Dragonfly)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    capung: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Capung
