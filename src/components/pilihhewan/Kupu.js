import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Butterfly from '../../assets/hewan/kupu.jpg'

const Kupu = () => {
    return (
        <View style={styles.container}>
            <Image source={Butterfly} style={styles.kupu}/>
            <Text style={styles.text}>Kupu-kupu</Text>
            <Text style={styles.text2}>(Butterfly)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kupu: {
        width: 299,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Kupu
