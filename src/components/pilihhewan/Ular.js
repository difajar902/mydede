import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Snake from '../../assets/hewan/ular.jpg'

const Ular = () => {
    return (
        <View style={styles.container}>
            <Image source={Snake} style={styles.ular}/>
            <Text style={styles.text}>Ular</Text>
            <Text style={styles.text2}>(Snake)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    ular: {
        width: 300,
        height: 175,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Ular
