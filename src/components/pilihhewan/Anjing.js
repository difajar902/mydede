import React from 'react'
import { View, Text, Image, StyleSheet} from 'react-native'
import Dog from '../../assets/hewan/anjing.jpg'

const Anjing = () => {
    return (
        <View style={styles.container}>
            <Image source={Dog} style={styles.anjing}/>
            <Text style={styles.text}>Anjing</Text>
            <Text style={styles.text2}>(Dog)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    anjing: {
        width: 250,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Anjing
