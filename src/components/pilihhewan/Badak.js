import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Rhino from '../../assets/hewan/badak.jpg'

const Badak = () => {
    return (
        <View style={styles.container}>
            <Image source={Rhino} style={styles.badak}/>
            <Text style={styles.text}>Badak</Text>
            <Text style={styles.text2}>(Rhino)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    badak: {
        width: 250,
        height: 200,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Badak
