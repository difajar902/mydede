import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Rabbit from '../../assets/hewan/kelinci.jpg'

const Kelinci = () => {
    return (
        <View style={styles.container}>
            <Image source={Rabbit} style={styles.kelinci}/>
            <Text style={styles.text}>Kelinci</Text>
            <Text style={styles.text2}>(Rabbit)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kelinci: {
        width: 350,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Kelinci
