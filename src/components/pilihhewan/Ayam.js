import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Chicken from '../../assets/hewan/Ayam.png'

const Ayam = () => {
    return (
        <View style={styles.container}>
            <Image source={Chicken} style={styles.ayam}/>
            <Text style={styles.text}>Ayam</Text>
            <Text style={styles.text2}>(Chicken)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    ayam: {
        width: 250,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Ayam
