import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Pand from '../../assets/hewan/panda.jpg'

const Panda = () => {
    return (
        <View style={styles.container}>
            <Image source={Pand} style={styles.panda}/>
            <Text style={styles.text}>Panda</Text>
            <Text style={styles.text2}>(Panda)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    panda: {
        width: 299,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Panda
