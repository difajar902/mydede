import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Duck from '../../assets/hewan/bebek.jpeg'

const Bebek = () => {
    return (
        <View style={styles.container}>
            <Image source={Duck} style={styles.bebek}/>
            <Text style={styles.text}>Bebek</Text>
            <Text style={styles.text2}>(Duck)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    bebek: {
        width: 260,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Bebek
