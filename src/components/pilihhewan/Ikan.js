import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Fish from '../../assets/hewan/ikan.png'

const Ikan = () => {
    return (
        <View style={styles.container}>
            <Image source={Fish} style={styles.ikan}/>
            <Text style={styles.text}>Ikan</Text>
            <Text style={styles.text2}>(Fish)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    ikan: {
        width: 350,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Ikan
