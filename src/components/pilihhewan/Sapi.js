import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Cow from '../../assets/hewan/sapi.jpg'

const Sapi = () => {
    return (
        <View style={styles.container}>
            <Image source={Cow} style={styles.sapi}/>
            <Text style={styles.text}>Sapi</Text>
            <Text style={styles.text2}>(Cow)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    sapi: {
        width: 299,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Sapi
