import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Cat from '../../assets/hewan/kucing.jpg'

const Kucing = () => {
    return (
        <View style={styles.container}>
            <Image source={Cat} style={styles.kucing}/>
            <Text style={styles.text}>Kucing</Text>
            <Text style={styles.text2}>(Cat)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kucing: {
        width: 350,
        height: 250,
        margin: 25,
        marginBottom: 1,
        marginLeft: 20
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Kucing
