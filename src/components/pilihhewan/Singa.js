import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Lion from '../../assets/hewan/singa.jpg'

const Singa = () => {
    return (
        <View style={styles.container}>
            <Image source={Lion} style={styles.singa}/>
            <Text style={styles.text}>Singa</Text>
            <Text style={styles.text2}>(Lion)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    singa: {
        width: 300,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Singa
