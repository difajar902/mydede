import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Ant from '../../assets/hewan/semut.webp'

const Semut = () => {
    return (
        <View style={styles.container}>
            <Image source={Ant} style={styles.semut}/>
            <Text style={styles.text}>Semut</Text>
            <Text style={styles.text2}>(Ant)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    semut: {
        width: 299,
        height: 250,
        margin: 25,
        marginBottom: 1,
        marginLeft: 100
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Semut
