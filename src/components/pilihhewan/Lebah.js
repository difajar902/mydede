import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Bee from '../../assets/hewan/lebah.jpg'

const Lebah = () => {
    return (
        <View style={styles.container}>
            <Image source={Bee} style={styles.lebah}/>
            <Text style={styles.text}>Lebah</Text>
            <Text style={styles.text2}>(Bee)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    lebah: {
        width: 320,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Lebah
