import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Zeb from '../../assets/hewan/zebra.png'

const Zebra = () => {
    return (
        <View style={styles.container}>
            <Image source={Zeb} style={styles.zebra}/>
            <Text style={styles.text}>Zebra</Text>
            <Text style={styles.text2}>(Zebra)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    zebra: {
        width: 290,
        height: 250,
        margin: 25,
        marginBottom: 1,
        marginLeft: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Zebra
