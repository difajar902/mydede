import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Ping from '../../assets/hewan/pinguin.jpg'

const Pinguin = () => {
    return (
        <View style={styles.container}>
            <Image source={Ping} style={styles.pinguin}/>
            <Text style={styles.text}>Pinguin</Text>
            <Text style={styles.text2}>(Penguin)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    pinguin: {
        width: 299,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Pinguin
