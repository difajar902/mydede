import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Elephant from '../../assets/hewan/gajah.jpg'

const Gajah = () => {
    return (
        <View style={styles.container}>
            <Image source={Elephant} style={styles.gajah}/>
            <Text style={styles.text}>Gajah</Text>
            <Text style={styles.text2}>(Elephant)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    gajah: {
        width: 335,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Gajah
