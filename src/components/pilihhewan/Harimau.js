import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Tiger from '../../assets/hewan/harimau.jpg'

const Harimau = () => {
    return (
        <View style={styles.container}>
            <Image source={Tiger} style={styles.harimau}/>
            <Text style={styles.text}>Harimau</Text>
            <Text style={styles.text2}>(Tiger)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    harimau: {
        width: 350,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Harimau
