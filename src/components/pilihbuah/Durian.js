import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Duri from '../../assets/buah/durian.jpg'

const Durian = () => {
    return (
        <View style={styles.container}>
            <Image source={Duri} style={styles.durian}/>
            <Text style={styles.text}>Durian</Text>
            <Text style={styles.text2}>(Durian)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    durian: {
        width: 295,
        height: 170,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Durian
