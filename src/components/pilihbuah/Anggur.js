import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Grape from '../../assets/buah/anggur.png'

const Anggur = () => {
    return (
        <View style={styles.container}>
            <Image source={Grape} style={styles.anggur}/>
            <Text style={styles.text}>Anggur</Text>
            <Text style={styles.text2}>(Grape)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    anggur: {
        width: 220,
        height: 180,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})

export default Anggur
