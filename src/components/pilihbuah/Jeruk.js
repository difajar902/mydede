import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Orange from '../../assets/buah/jeruk.jpg'

const Jeruk = () => {
    return (
        <View style={styles.container}>
            <Image source={Orange} style={styles.jeruk}/>
            <Text style={styles.text}>Jeruk</Text>
            <Text style={styles.text2}>(Orange)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    jeruk: {
        width: 295,
        height: 170,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Jeruk
