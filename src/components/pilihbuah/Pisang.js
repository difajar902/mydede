import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Banana from '../../assets/buah/pisang.jpg'

const Pisang = () => {
    return (
        <View style={styles.container}>
            <Image source={Banana} style={styles.pisang}/>
            <Text style={styles.text}>Pisang</Text>
            <Text style={styles.text2}>(Banana)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    pisang: {
        width: 265,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Pisang
