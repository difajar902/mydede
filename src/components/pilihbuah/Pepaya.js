import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Papaya from '../../assets/buah/pepaya1.jpg'

const Pepaya = () => {
    return (
        <View style={styles.container}>
            <Image source={Papaya} style={styles.pepaya}/>
            <Text style={styles.text}>Pepaya</Text>
            <Text style={styles.text2}>(Papaya)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    pepaya: {
        width: 280,
        height: 230,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Pepaya
