import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Watermelon from '../../assets/buah/semangka.webp'

const Semangka = () => {
    return (
        <View style={styles.container}>
            <Image source={Watermelon} style={styles.semangka}/>
            <Text style={styles.text}>Semangka</Text>
            <Text style={styles.text2}>(Watermelon)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    semangka: {
        width: 300,
        height: 200,
        margin: 25,
        marginBottom: 0,
        marginLeft: 30
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Semangka
