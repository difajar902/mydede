import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Coconut from '../../assets/buah/kelapa.png'

const Kelapa = () => {
    return (
        <View style={styles.container}>
            <Image source={Coconut} style={styles.kelapa}/>
            <Text style={styles.text}>Kelapa</Text>
            <Text style={styles.text2}>(Coconut)</Text>
        </View>       
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kelapa: {
        width: 280,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Kelapa
