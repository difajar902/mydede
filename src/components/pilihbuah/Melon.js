import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Mel from '../../assets/buah/melon.jpg'

const Melon = () => {
    return (
        <View style={styles.container}>
            <Image source={Mel} style={styles.melon}/>
            <Text style={styles.text}>Melon</Text>
            <Text style={styles.text2}>(Melon)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    melon: {
        width: 230,
        height: 230,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Melon
