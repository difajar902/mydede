import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Avocado from '../../assets/buah/alpukat.png'

const Alpukat = () => {
    return (
        <View style={styles.container}>
            <Image source={Avocado} style={styles.alpukat}/>
            <Text style={styles.text}>Alpukat</Text>
            <Text style={styles.text2}>(Avocado)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    alpukat: {
        width: 220,
        height: 180,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Alpukat
