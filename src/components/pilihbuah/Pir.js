import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Pi from '../../assets/buah/pir.png'

const Pir = () => {
    return (
        <View style={styles.container}>
            <Image source={Pi} style={styles.pir}/>
            <Text style={styles.text}>Pir</Text>
            <Text style={styles.text2}>(Pear)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    pir: {
        width: 240,
        height: 230,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Pir
