import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Strawberry from '../../assets/buah/strawberry.jpg'

const Stroberi = () => {
    return (
        <View style={styles.container}>
            <Image source={Strawberry} style={styles.stroberi}/>
            <Text style={styles.text}>Stroberi</Text>
            <Text style={styles.text2}>(Strawberry)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    stroberi: {
        width: 300,
        height: 200,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Stroberi
