import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Kiw from '../../assets/buah/kiwi.png'

const Kiwi = () => {
    return (
        <View style={styles.container}>
            <Image source={Kiw} style={styles.kiwi}/>
            <Text style={styles.text}>Kiwi</Text>
            <Text style={styles.text2}>(Kiwi)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kiwi: {
        width: 280,
        height: 180,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Kiwi
