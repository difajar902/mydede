import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Cherry from '../../assets/buah/ceri.png'

const Ceri = () => {
    return (
        <View style={styles.container}>
            <Image source={Cherry} style={styles.ceri}/>
            <Text style={styles.text}>Ceri</Text>
            <Text style={styles.text2}>(Cherry)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    ceri: {
        width: 260,
        height: 180,
        margin: 25,
        marginBottom: 1,
        marginLeft: 50
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Ceri
