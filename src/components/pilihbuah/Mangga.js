import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Mango from '../../assets/buah/mangga.png'

const Mangga = () => {
    return (
        <View style={styles.container}>
            <Image source={Mango} style={styles.mangga}/>
            <Text style={styles.text}>Mangga</Text>
            <Text style={styles.text2}>(Mango)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    mangga: {
        width: 230,
        height: 230,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }  
})
export default Mangga
