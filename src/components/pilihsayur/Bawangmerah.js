import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Shallot from '../../assets/sayur/bawangmerah.jpg'

const Bawangmerah = () => {
    return (
        <View style={styles.container}>
            <Image source={Shallot} style={styles.bawangmerah}/>
            <Text style={styles.text}>Bawang Merah</Text>
            <Text style={styles.text2}>(Shallot)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    bawangmerah: {
        width: 285,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})   
export default Bawangmerah
