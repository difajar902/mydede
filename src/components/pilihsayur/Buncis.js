import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Snaps from '../../assets/sayur/buncis.jpg'

const Buncis = () => {
    return (
        <View style={styles.container}>
            <Image source={Snaps} style={styles.buncis}/>
            <Text style={styles.text}>Buncis</Text>
            <Text style={styles.text2}>(Snaps)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    buncis: {
        width: 285,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Buncis
