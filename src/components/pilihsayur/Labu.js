import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Pumpkin from '../../assets/sayur/labu.png'

const Labu = () => {
    return (
        <View style={styles.container}>
            <Image source={Pumpkin} style={styles.labu}/>
            <Text style={styles.text}>Labu</Text>
            <Text style={styles.text2}>(Pumpkin)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    labu: {
        width: 290,
        height: 260,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Labu
