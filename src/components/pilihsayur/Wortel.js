import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Carrot from '../../assets/sayur/wortel.png'

const Wortel = () => {
    return (
        <View style={styles.container}>
            <Image source={Carrot} style={styles.wortel}/>
            <Text style={styles.text}>Wortel</Text>
            <Text style={styles.text2}>(Carrot)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    wortel: {
        width: 330,
        height: 200,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Wortel
