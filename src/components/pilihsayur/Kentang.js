import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Potato from '../../assets/sayur/kentang.png'

const Kentang = () => {
    return (
        <View style={styles.container}>
            <Image source={Potato} style={styles.kentang}/>
            <Text style={styles.text}>Kentang</Text>
            <Text style={styles.text2}>(Potato)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kentang: {
        width: 290,
        height: 270,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Kentang
