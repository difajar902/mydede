import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Longbean from '../../assets/sayur/kacangpanjang.jpg'

const Kacangpanjang = () => {
    return (
        <View style={styles.container}>
            <Image source={Longbean} style={styles.kacangpanjang}/>
            <Text style={styles.text}>Kacang Panjang</Text>
            <Text style={styles.text2}>(Long Bean)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kacangpanjang: {
        width: 280,
        height: 280,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Kacangpanjang
