import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Broccoli from '../../assets/sayur/brokoli.jpg'

const Brokoli = () => {
    return (
        <View style={styles.container}>
            <Image source={Broccoli} style={styles.brokoli}/>
            <Text style={styles.text}>Brokoli</Text>
            <Text style={styles.text2}>(Broccoli)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    brokoli: {
        width: 274,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Brokoli
