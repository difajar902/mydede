import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Mustardgreen from '../../assets/sayur/sawi.jpg'

const Sawi = () => {
    return (
        <View style={styles.container}>
            <Image source={Mustardgreen} style={styles.sawi}/>
            <Text style={styles.text}>Sawi Hijau</Text>
            <Text style={styles.text2}>(Mustard Green)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    sawi: {
        width: 290,
        height: 240,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Sawi
