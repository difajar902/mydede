import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Onion from '../../assets/sayur/bombay.jpg'

const Bombay = () => {
    return (
        <View style={styles.container}>
            <Image source={Onion} style={styles.bombay}/>
            <Text style={styles.text}>Bawang Bombay</Text>
            <Text style={styles.text2}>(Onion)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    bombay: {
        width: 320,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Bombay
