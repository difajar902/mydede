import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Cauliflower from '../../assets/sayur/kembangkol.jpg'

const Bungakol = () => {
    return (
        <View style={styles.container}>
            <Image source={Cauliflower} style={styles.kol}/>
            <Text style={styles.text}>Bunga Kol</Text>
            <Text style={styles.text2}>(Cauliflower)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kol: {
        width: 274,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Bungakol
