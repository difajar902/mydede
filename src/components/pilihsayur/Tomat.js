import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Tomato from '../../assets/sayur/tomat.png'

const Tomat = () => {
    return (
        <View style={styles.container}>
            <Image source={Tomato} style={styles.tomat}/>
            <Text style={styles.text}>Tomat</Text>
            <Text style={styles.text2}>(Tomato)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    tomat: {
        width: 260,
        height: 250,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Tomat
