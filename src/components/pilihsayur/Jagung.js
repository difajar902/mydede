import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Corn from '../../assets/sayur/jagung.png'

const Jagung = () => {
    return (
        <View style={styles.container}>
            <Image source={Corn} style={styles.jagung}/>
            <Text style={styles.text}>Jagung</Text>
            <Text style={styles.text2}>(Corn)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    jagung: {
        width: 280,
        height: 280,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Jagung
