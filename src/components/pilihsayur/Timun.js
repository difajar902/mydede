import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Cucumber from '../../assets/sayur/timun.jpg'

const Timun = () => {
    return (
        <View style={styles.container}>
            <Image source={Cucumber} style={styles.timun}/>
            <Text style={styles.text}>Timun</Text>
            <Text style={styles.text2}>(Cucumber)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    timun: {
        width: 330,
        height: 190,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Timun
