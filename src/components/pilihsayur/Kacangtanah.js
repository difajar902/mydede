import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Peanut from '../../assets/sayur/kacangtanah.jpg'

const Kacangtanah = () => {
    return (
        <View style={styles.container}>
            <Image source={Peanut} style={styles.kacangtanah}/>
            <Text style={styles.text}>Kacang Tanah</Text>
            <Text style={styles.text2}>(Peanut)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    kacangtanah: {
        width: 260,
        height: 260,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Kacangtanah
