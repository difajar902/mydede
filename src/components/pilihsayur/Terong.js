import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Eggplant from '../../assets/sayur/terong.jpg'

const Terong = () => {
    return (
        <View style={styles.container}>
            <Image source={Eggplant} style={styles.terong}/>
            <Text style={styles.text}>Terong</Text>
            <Text style={styles.text2}>(Eggplant)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    terong: {
        width: 240,
        height: 240,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Terong
