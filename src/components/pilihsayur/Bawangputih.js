import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Garlic from '../../assets/sayur/bawangputih.png'

const Bawangputih = () => {
    return (
        <View style={styles.container}>
            <Image source={Garlic} style={styles.bawangputih}/>
            <Text style={styles.text}>Bawang Putih</Text>
            <Text style={styles.text2}>(Garlic)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    bawangputih: {
        width: 274,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})    
export default Bawangputih
