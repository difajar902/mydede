import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Chili from '../../assets/sayur/cabai.png'

const Cabai = () => {
    return (
        <View style={styles.container}>
            <Image source={Chili} style={styles.cabai}/>
            <Text style={styles.text}>Cabai</Text>
            <Text style={styles.text2}>(Chili)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    cabai: {
        width: 274,
        height: 220,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Cabai
