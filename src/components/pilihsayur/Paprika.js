import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Pap from '../../assets/sayur/paprika.jpg'

const Paprika = () => {
    return (
        <View style={styles.container}>
            <Image source={Pap} style={styles.paprika}/>
            <Text style={styles.text}>Paprika</Text>
            <Text style={styles.text2}>(Paprika)</Text>
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 150,
        backgroundColor: 'white'
    },
    paprika: {
        width: 290,
        height: 230,
        margin: 25,
        marginBottom: 1
    },
    text: {
        textAlign: 'center',
        fontSize: 30,
    },
    text2: {
        textAlign: 'center',
        fontSize: 30,
        marginBottom: 250
    }
})
export default Paprika
